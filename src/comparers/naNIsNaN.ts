export function naNIsNaN<T>(a:T, b:T) {
  if (typeof a === "number" && isNaN(a) && typeof b === "number" && isNaN(b)) {
    return {diff: null}
  }

  return null
}