import { diff } from "../..";

export function equalObjectsRelaxed<T>(a:T, b:T, options?:diff.Options<T>) {
  if (!isObject(a) || !isObject(b)) {
    return null
  }

  let isDirty = false
  const result = {}
  const keys = getKeys(a, b, options)

  if (isArray(a) && isArray(b)) {
    const aArray = asSortedKeysArray(a, keys)
    const bArray = asSortedKeysArray(b, keys)

    if (aArray.length != bArray.length) {
      isDirty = true
      result["length"] = false
    }

    const minLen = Math.min(aArray.length, bArray.length)
    const maxLen = Math.max(aArray.length, bArray.length)
    for(let i = 0; i < maxLen; i++) {
      const aValue = aArray[i]
      const bValue = bArray[i]
      if (aValue != bValue) {
        isDirty = true
        result[i] = false
      }
    }

    for(let i = minLen; i < maxLen; i++) {
      isDirty = true
      result[i] = false
    }
  }

  for(const key of keys) {
    const aValue = a[key]
    const bValue = b[key]

    const subOptions = diff.deriveOptionsForKey(key, options)
    const itemResult = diff(aValue, bValue, subOptions)
    if (itemResult == null) {
      continue
    }

    isDirty = true
    result[key] = itemResult
  }

  return {
    diff: isDirty ? result : null
  }
}

function isPrimitive(input):input is undefined | bigint | boolean | number | string {
  switch(typeof input) {
    case "bigint":
    case "boolean":
    case "number":
    case "string":
    case "undefined":
      return true
  }

  return false
}

function isObject(input):input is object {
  return typeof input === "object"
}

function isArray(input):input is Array<unknown> {
  return Array.isArray(input)
}

function isArrayIndex<T>(array:T[], possibleIndex):possibleIndex is number {
  let floatNum:number
  let integerNum:number

  switch(typeof possibleIndex) {
    case "number": {
      floatNum = possibleIndex
      integerNum = possibleIndex
      break
    }
    case "string": {
      floatNum = parseFloat(possibleIndex)
      if (isNaN(floatNum)) {
        return false
      }

      integerNum = parseInt(possibleIndex)
      if (isNaN(integerNum)) {
        return false
      }
      break
    }
    default:
      return false
  }

  if (floatNum != integerNum) {
    return false
  }

  if (integerNum >= 0 && integerNum < array.length) {
    return true
  }

  return false
}

function getKeys<T>(a:T, b:T, options?:diff.Options<T>) {
  const objOptions = options as diff.ObjectOptions<T>

  let aKeys = Object.keys(a)
  let bKeys = Object.keys(b)

  if (isArray(a) && isArray(b)) {
    aKeys = aKeys.filter(x => !isArrayIndex(a, x))
    bKeys = bKeys.filter(x => !isArrayIndex(b, x))
  }
  const abKeys = new Array(...new Set(["length"].concat(aKeys).concat(bKeys)))

  let result = abKeys

  const onlyKeys = objOptions?.only
  if (onlyKeys) {
    result = result.filter(x => onlyKeys.includes(x))
  }

  const exceptKeys = objOptions?.except
  if (exceptKeys && exceptKeys.length > 0) {
    result = result.filter(x => !exceptKeys.includes(x))
  }

  return result
}

function asSortedKeysArray<T>(input:Array<T>, keys:PropertyKey[]) {
  if (input === undefined || input == null) {
    return input
  }

  return keys.sort().map(key => `${String(key)}:${stringify(input[key])}`)
}

function stringify(input):string {
  if (input == null) return "null"

  switch(typeof input) {
    case "object":
    case "function":
      const keys = Object.keys(input).sort()
      const entries = keys.map(key => [key, stringify(input[key])])
      const tmpObject = Object.fromEntries(entries)
      return JSON.stringify(tmpObject)
  }

  return JSON.stringify(input)
}
