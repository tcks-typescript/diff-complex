import { diff } from "../..";

export function equalObjects<T>(a:T, b:T, options?:diff.Options<T>) {
  if (!isObject(a) || !isObject(b)) {
    return null
  }

  let isDirty = false
  const result = {}
  const keys = getKeys(a, b, options)
  for(const key of keys) {
    const aValue = a[key]
    const bValue = b[key]

    const subOptions = diff.deriveOptionsForKey(key, options)
    const itemResult = diff(aValue, bValue, subOptions)
    if (itemResult == null) {
      continue
    }

    isDirty = true
    result[key] = itemResult
  }

  return {
    diff: isDirty ? result : null
  }
}

function isObject(input):input is object {
  return typeof input === "object"
}

function getKeys<T>(a:T, b:T, options?:diff.Options<T>) {
  const objOptions = options as diff.ObjectOptions<T>

  const aKeys = Object.keys(a)
  const bKeys = Object.keys(b)
  const abKeys = new Array(...new Set(aKeys.concat(bKeys)))

  let result = abKeys

  const onlyKeys = objOptions?.only
  if (onlyKeys) {
    result = result.filter(x => onlyKeys.includes(x))
  }

  const exceptKeys = objOptions?.except
  if (exceptKeys && exceptKeys.length > 0) {
    result = result.filter(x => !exceptKeys.includes(x))
  }

  return result
}
