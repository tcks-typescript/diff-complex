export function equalDates<T>(a:T, b:T) {
  const aIsDate = a instanceof Date
  const bIsDate = b instanceof Date

  if (!aIsDate || !bIsDate) {
    return null
  }

  const aValue = a.valueOf()
  const bValue = b.valueOf()

  if (aValue == bValue) {
    return {diff: null}
  }
  else {
    return false
  }
}