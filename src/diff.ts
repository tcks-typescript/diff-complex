import { Comparer, getDefault, getRelaxed, getStrict } from "./comparers"

export function diff<T>(a:T, b:T, options?:diff.Options<T> | null) : diff.Result<T> | null {
  if (a === b) {
    return null
  }

  const prepare = options?.prepare
  if (prepare) {
    prepare(a)
    prepare(b)
  }

  const comparers = getComparers(options?.comparers)
  for(const comparer of comparers) {

    const result = comparer(a, b, options)
    if (result === false) {
      return false
    }

    if (result) {
      return result.diff
    }
  }

  // this is fallback. none of comparers was able to compare it
  return false
}

function getComparers<T>(input?:diff.BaseOptions<T>["comparers"]) {
  switch(input) {
    case null:
    case undefined:
    case "default":
      return getDefault()
    case "strict":
      return getStrict()
    case "relaxed":
      return getRelaxed()
    default:
      return input ?? []
  }
}

export module diff {
  export type Options<T>
    = BaseOptions<T>
    & (T extends object ? ObjectOptions<T>
        : ScalarOptions<T>)

  export type BaseOptions<T> = {
    comparers?: Comparer<T>[] | "default" | "strict" | "relaxed" | null
    prepare?: (obj:T) => void
  }

  export type ScalarOptions<T> = BaseOptions<T> & {
  }

  export type ObjectOptions<T> = BaseOptions<T> & {
    only?: PropertyKey[],
    except?: PropertyKey[]
    with?: {
      [P in keyof T]?: Options<T[P]>
    }
  } & (
    T extends Array<infer E>
      ? ArrayOnlyOptions<E>
      : { }
  )

  export type ArrayOnlyOptions<E> = {
    sort?: null | true
          | ((a:E, b:E) => number)
          | {by: string | string[] | ((x:E) => unknown) | ((x:E) => unknown)}
  }

  export function deriveOptionsForKey<T>(key:PropertyKey, options?:Options<T>):Options<T> | null {
    if (options == null) {
      return null
    }

    const objOptions = options as ObjectOptions<T>

    const spec = objOptions.with?.[key]
    const result = spec ? Object.assign({}, spec) : { }
    if (result.comparers == null) {
      result.comparers = options.comparers
    }

    return result as Options<T>
  }
}

export module diff {
  export type Result<T>
    = ScalarResult
    | ObjectResult<T>

  export type ScalarResult = false

  export type ObjectResult<T> = {
    [P in keyof T]?: Result<T>
  }
}
