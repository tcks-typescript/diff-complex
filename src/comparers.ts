import { diff } from ".";
import * as f from "./comparers/.index"

/**
 * Represents unified signature for comparision functions.
 * 
 * @returns
 * null - comparer can not compare those two values
 * false - those two values are different
 * {diff: null} - those two values are equal
 * {diff: Result<T>} - those two values are different (with specified diferencies)
 */
export type Comparer<T>
  = (a:T, b:T, options?:diff.Options<T> | null)
    => null
      | false
      | {diff: null | diff.Result<T>}

export function list<T>():Array<[Comparer<T>] | [Comparer<T>, null | "default" | "strict"]> {
  return [
    [f.equals],
    [f.naNIsNaN, "strict"],

    [f.Dates.equalDates, "strict"],

    [f.Objects.equalObjects, "strict"]
  ]
}

let defaultSource:Comparer<unknown>[]
export function getDefault<T>():Comparer<T>[] {
  return new Array(...(defaultSource ?? (
    defaultSource = list<T>().map(x => x[0])
  )))
}

let strictSource:Comparer<unknown>[]
export function getStrict<T>():Comparer<T>[] {
  return new Array(...strictSource ?? (
    strictSource = list<T>().filter(x => x[1] == "strict").map(x => x[0])
  ))
}

let relaxedSource:Comparer<unknown>[]
export function getRelaxed<T>():Comparer<T>[] {
  return new Array(...(relaxedSource ?? (
    relaxedSource = makeRelaxed()
  )))
}

function makeRelaxed<T>():Comparer<T>[] {
  const result = getDefault()
  const ndx = result.findIndex(x => x === f.Objects.equalObjects)
  if (ndx < 0) {
    throw new Error("Can not find comparer to replace.")
  }
  result[ndx] = f.Objects.equalObjectsRelaxed

  return result
}