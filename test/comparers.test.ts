import { getDefault, getStrict } from "../src/comparers"

describe("comparers", () => {
  test("strict mode preserves order of comparers same as in default mode", () => {
    const arrDefault = getDefault()
    const arrStrict = getStrict()
    expect(arrDefault.length).toBeGreaterThan(arrStrict.length)

    const arrDefault2 = arrDefault.filter(x => arrStrict.includes(x))
    const arrStrict2 = arrStrict.filter(x => arrDefault.includes(x))

    expect(arrStrict.length).toBe(arrStrict2.length)
    expect(arrDefault2.length).toBe(arrStrict2.length)

    for(let i = 0; i < arrStrict2.length; i++) {
      expect(arrStrict2[i]).toBe(arrDefault2[i])
    }
  })
})