import { diff } from "../src"

describe("diff - objects", () => {
  it("returns null for two instances of empty objects", ()=> {
    expect(diff({}, {})).toBeNull()
    expect(diff({}, {}, {comparers: "strict"})).toBeNull()
  })

  it("returns null for two instances with same content", ()=> {
    function makeNew() {
      return {
        a: null, b: true, c: false, d: "kuku", e: new Date(),
        f: {
          a: "lala",
          b: [],
          c: [[null, 1], ["kuku"], [[{a: false}]]]
        }
      }
    }

    const a = makeNew()
    const b = makeNew()
    expect(a === b).toBe(false)

    expect(diff(a, b)).toBeNull()
    expect(diff(a, b, {comparers: "strict"})).toBeNull()
  })

  it("returns diff for two instances with differencies", () => {
    const a = {a: false, b: true, c: "kuku"}
    const b = {a: true, b: true, c: "lala"}

    expect(diff(a, b)).toEqual({
      a: false,
      c: false
    })

    expect(diff(a, b, {comparers: "strict"})).toEqual({
      a: false,
      c: false
    })


    const d = {a: {b: {c: [null, true, 2]}}}
    const e = {a: {b: {c: [null, false, 2]}}}

    expect(diff(d, e)).toEqual({
      a: {b: {c: {"1": false}}}
    })
    expect(diff(d, e, {comparers: "strict"})).toEqual({
      a: {b: {c: {"1": false}}}
    })
  })

  describe("keys filtering", () => {
    it("ignores except keys on top level", () => {
      const a = {name: "Alice", gender: "F", age: 25}
      const b = {name: "Beata", gender: "F", age: 25}

      expect(diff(a, b, {
        except: ["name"]
      })).toBeNull()

      expect(diff(a, b, {
        comparers: "strict",
        except: ["name"]
      })).toBeNull()
    })

    it("count with only keys on top level", () => {
      const a = {name: "Alice", gender: "F", age: 25}
      const b = {name: "Beata", gender: "F", age: 25}

      expect(diff(a, b, {
        only: ["gender", "age"]
      })).toBeNull()

      expect(diff(a, b, {
        comparers: "strict",
        only: ["gender", "age"]
      })).toBeNull()
    })

    it("count with both only and except keys on top level", () => {
      const a = {name: "Alice", gender: "F", age: 25}
      const b = {name: "Beata", gender: "F", age: 39}

      expect(diff(a, b, {
        only: ["gender", "age"],
        except: ["age"]
      })).toBeNull()

      expect(diff(a, b, {
        comparers: "strict",
        only: ["gender", "age"],
        except: ["age"]
      })).toBeNull()
    })

    it("ignores except-keys only on top level", () => {
      const a = {name: "Alice", age: 25, dog: {name: "Zog", age: 3}}
      const b = {name: "Bruno", age: 25, dog: {name: "Ypr", age: 3}}

      expect(diff(a, b, {
        except: ["name"]
      })).toEqual({dog: {name: false}})
    })

    it("filters only-keys only on top level", () => {
      const a = {name: "Alice", age: 25, dog: {name: "Zog", age: 3}}
      const b = {name: "Bruno", age: 25, dog: {name: "Ypr", age: 4}}

      expect(diff(a, b, {
        only: ["age", "dog"]
      })).toEqual({dog: {name: false, age: false}})
    })

    it("ignores except-keys on second level", () => {
      const a = {name: "Alice", dog: {name: "Spot", age: 7, race: {name: "Doga"}}}
      const b = {name: "Bruno", dog: {name: "Bary", age: 7, race: {name: "Pitbul"}}}

      expect(diff(a, b, {
        with: {
          dog: {
            except: ["name"]
          }
        }
      })).toEqual({
        name: false,
        dog: {
          race: {
            name: false
          }
        }
      })
    })
  })
})
