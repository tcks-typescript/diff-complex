import { diff } from "../src"

describe("diff - scalars", () => {
  describe("null/undefined", () => {
    it("returns null with (null, null)", () => {
      const result = diff(null, null)
      expect(result).toBe(null)
    })

    it("returns null with (undefined, undefined)", () => {
      const result = diff(undefined, undefined)
      expect(result).toBe(null)
    })

    it("returns null with (null, undefined)", () => {
      const result = diff(null, undefined)
      expect(result).toBe(null)
    })

    it("returns null with (undefined, null)", () => {
      const result = diff(undefined, null)
      expect(result).toBe(null)
    })

    it("returns false with (null, undefined) in strict mode", () => {
      const result = diff(null, undefined, { comparers: "strict" })
      expect(result).toBe(false)
    })

    it("returns false with (undefined, null) in strict mode", () => {
      const result = diff(undefined, null, { comparers: "strict" })
      expect(result).toBe(false)
    })
  })

  describe("numbers", () => {
    it("return null with regular numbers", () => {
      const numbers = new Array(50).map(x => Math.random())
      for(const number of numbers) {
        const result = diff(number, number)
        expect(result).toBe(null)
      }
    })

    it("return null with regular numbers in strict mode", () => {
      const numbers = new Array(50).map(x => Math.random())

      for(const number of numbers) {
        const result = diff(number, number, {comparers: "strict"})
        expect(result).toBe(null)
      }
    })

    test("returns null with special numbers", () => {
      const numbers = getSpecialNumbers()

      for(const number of numbers) {
        const result = diff(number, number)
        expect(result).toBe(null)
      }
    })

    test("returns null with special numbers in strict mode", () => {
      const numbers = getSpecialNumbers()

      for(const number of numbers) {
        const result = diff(number, number, {comparers: "strict"})
        expect(result).toBe(null)
      }
    })

    test("returns false with special numbers", () => {
      const numbers = getSpecialNumbers()

      for(const number of numbers) {
        const result = diff(number, 5)
        expect(result).toBe(false)
      }
    })
  })

  describe("booleans", () => {
    it("returns null on same values", () => {
      expect(diff(false, false)).toBe(null)
      expect(diff(true, true)).toBe(null)
    })

    it("returns false on different values", () => {
      expect(diff(false, true)).toBe(false)
      expect(diff(false, true)).toBe(false)
    })
  })

  describe("bigints", () => {
    it("returns null with same values", () => {
      expect(diff(BigInt(0), BigInt(0))).toBeNull()
      expect(diff(BigInt(1), BigInt(1))).toBeNull()
      expect(diff(BigInt(-1), BigInt(-1))).toBeNull()
      expect(diff(BigInt(2), BigInt(2))).toBeNull()
      expect(diff(BigInt(-2), BigInt(-2))).toBeNull()
    })

    it("returns null with same values in strict mode", () => {
      expect(diff(BigInt(0), BigInt(0), {comparers: "strict"})).toBeNull()
      expect(diff(BigInt(1), BigInt(1), {comparers: "strict"})).toBeNull()
      expect(diff(BigInt(-1), BigInt(-1), {comparers: "strict"})).toBeNull()
      expect(diff(BigInt(2), BigInt(2), {comparers: "strict"})).toBeNull()
      expect(diff(BigInt(-2), BigInt(-2), {comparers: "strict"})).toBeNull()
    })

    it("returns false with different values", () => {
      expect(diff(BigInt(0), BigInt(1))).toBe(false)
      expect(diff(BigInt(1), BigInt(2))).toBe(false)
      expect(diff(BigInt(-1), BigInt(-3))).toBe(false)
      expect(diff(BigInt(2), BigInt(4))).toBe(false)
      expect(diff(BigInt(-2), BigInt(-5))).toBe(false)
    })
  })

  describe("strings", () => {
    it("returns null with differently created strings but with same characters", () => {
      const a = "he" + "llo"
      const b = "h" + "e" + "l" + "l" + "o"

      expect(diff(a, b)).toBeNull()
      expect(diff(a, b, {comparers: "strict"})).toBeNull()
    })
  })

  describe("dates", () => {
    it("returns true for two instances of same date", () => {
      const now = Date.now()

      const a = new Date(now)
      const b = new Date(now)
      expect(a === b).toBe(false)

      expect(diff(a, b)).toBeNull()
      expect(diff(a, b, {comparers: "strict"})).toBeNull()
    })
  })
})

function getSpecialNumbers() {
  return [
    Number.EPSILON,
    Number.MAX_SAFE_INTEGER,
    Number.MAX_VALUE,
    Number.MIN_SAFE_INTEGER,
    Number.MIN_VALUE,
    Number.NEGATIVE_INFINITY,
    Number.NaN,
    Number.POSITIVE_INFINITY
  ]
}