import { diff } from "../src"

describe("diff - arrays", () => {
  it("returns null for two empty arrays", () => {
    expect(diff<unknown>([], [])).toBeNull()
    expect(diff<unknown>([], [], {comparers: "strict"})).toBeNull()
  })

  it("returns null for two non-empty arrays", () => {
    expect(diff<unknown>([null, 1, "two"], [null, 1, "two"])).toBeNull()
    expect(diff<unknown>([null, 1, "two"], [null, 1, "two"], {comparers: "strict"})).toBeNull()
  })

  it("returns diff for two non-empty arrays with same values in different order", () => {
    expect(diff<unknown>([null, 1, "two"], [null, "two", 1])).toEqual({
      "1": false,
      "2": false
    })
    expect(diff<unknown>([null, 1, "two"], [null, "two", 1], {comparers: "strict"})).toEqual({
      "1": false,
      "2": false
    })
  })

  it("returns null for arrays of arrays", () => {
    expect(diff<unknown>([[]], [[]])).toBeNull()
    expect(diff<unknown>([[]], [[]], {comparers: "strict"})).toBeNull()
  })

  it("returns diff for arrays of arrays", () => {
    expect(diff<unknown>([[], [null], [0, 1, 2]], [[], [null], [0, 1, "two"]])).toEqual({
      "2": {
        "2": false
      }
    })
    expect(diff<unknown>([[], [null], [0, 1, 2]], [[], [null], [0, 1, "two"]], {comparers: "strict"})).toEqual({
      "2": {
        "2": false
      }
    })
  })
})