import { diff } from "../../src"

describe("diff - arrays - relaxed", () => {
  it("returns null for two disordered arrays with scalars", () => {
    const a = [null, 1, 2]
    const b = [2, null, 1]

    // @ts-ignore: [TS-BUG] Type instantiation is excessively deep and possibly infinite.
    expect(diff(a, b, {comparers: "relaxed"})).toBeNull()
  })

  it("returns null for two disordered arrays with records", () => {
    const a = [
      {id: 1, name: "one"},
      {id: 2, name: "two"},
      {id: 3, name: "three"},
      {id: 4, name: "four"}
    ]
    const b = JSON.parse(JSON.stringify(a)).reverse() as Array<typeof a[0]>

    // @ts-ignore: [TS-BUG] Type instantiation is excessively deep and possibly infinite.
    expect(diff(a, b, {
      comparers: "relaxed",
    })).toBeNull()
  })
})
