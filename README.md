# Diff-Complex

Diff-Complex is a TypeScript library for ultimate diffing complex objects and arrays.

## Installation


```bash
npm install diff-complex
```

## Usage

```typescript
import { diff } from "diff-complex"

// returns null. the null means "no differences"
diff({name: "Alice", age: 25}, {name: "Alice", age: 25})

// returns {name: false}
// because the values of "name" properties are different
diff({name: "Alice", age: 25}, {name: "Bruno", age: 25})

// returns {dog: {name: false}}
// because the values of properties are different on the path "dog.name"
diff(
  {name: "Alice", dog: {name: "Spot"}},
  {name: "Alice", dog: {name: "Rony"}}
)

```

### Default vs. Strict comparers
```typescript
// returns null
// because with default comparers the "null" and "undefined" are treated as same values
diff({name: null}, {name: undefined})

// returns {name: false}
// because with strict comparers the "null" and "undefined" are threated as different values
diff({name: null}, {name: undefined}, {comparers: "strict"})
```

### Arrays

```typescript
// returns null
diff([0, 1, 2], [0, 1, 2])

// returns {2: false, 3: false}
// because with default comparers the order of items in array matter
diff([0, 1, 2], [0, 2, 1])

// returns null
// because with relaxed comparers the order of items in array doesn't matter
diff([0, 1, 2], [0, 2, 1], comparers: "relaxed")
```

### Nested objects

```typescript
// returns {dog: {name: false}}
// the property on path "name" is ignored, therefore only other differences are included in result
diff(
  {name: "Alice", dog: {name: "Spot"}},
  {name: "Bruno", dog: {name: "Rony"}},
  {
    except: ["name"]
  }
)

// returns {dog: {name: false}, cat: {name: false}}
// only the hiearachy under the paths "dog" and "cat" is included in diff analysis
diff(
  {name: "Alice", age: 25, dog: {name: "Spot"}, cat: {name: "Mimi"}},
  {name: "Bruno", age: 31, dog: {name: "Rony"}, cat: {name: "Quti"}},
  {
    only: ["dog", "cat"]
  }
)

// returns {name: false, dog: {age: false}, cat: {name: false}}
// in the "dog" path is included only "age" property
// in the "cat" path is excluded "age" property
diff(
  {name: "Alice", age: 25,
    dog: {name: "Spot", age: 3},
    cat: {name: "Mimi", age: 2}
  },
  {name: "Bruno", age: 31,
    dog: {name: "Rony", age: 7},
    cat: {name: "Quti", age: 5}
  },
  {
    with: {
      dog: {only:["age"]},
      cat: {except:["age"]}
    }
  }
)
```

### Arrays with objects

```typescript
// returns {"1": false, "2": false}
diff([
  {id: 1, name: "Alice"}, {id: 2, name: "Bruno"}
],
[
  {id: 2, name: "Bruno"}, {id: 1, name: "Alice"}
])

// returns null
diff([
  {id: 1, name: "Alice"}, {id: 2, name: "Bruno"}
],
[
  {id: 2, name: "Bruno"}, {id: 1, name: "Alice"}
],
{
  // this will sort the arrays
  pre: (a, b) => a.id - b.id
})
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
